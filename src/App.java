import java.util.Scanner;

/**
 * Memisahkan kata dari suatu string
 * 
 * @author Aryo
 */
public class App {

    /**
     * Ini adalah modul utama yang akan mengeksekusi semua sintax
     * 
     * @param args unused
     * @throws Exception unused
     */
    public static void main(String[] args) throws Exception {
        Scanner keybScanner = new Scanner(System.in);

        String[] Split;
        String Input;

        Input = keybScanner.nextLine();
        keybScanner.close();

        Split = Input.split("[ !,?._'@]+");
        /* Fungsinya adalah dengan memisahkan string input menjadi kata yang berbeda */
        System.out.println(Split.length);
        for (String text : Split) {
            System.out.println(text);
        }
    }
}
